""" Pictorem

Video file upscaler using waifu2x algorithm. Design for use on macOS in conjunction with the CLI
    version of imxieyi/waifu2x-mac.

TODO:
    * Cleanup the semaphore usage here, could be a bit better
    * Better counting mechanism, shouldn't need to rely on checking if the frame exists always
    * Silent option, or have the main thread report the progress at pre-set intervals
        * Might have to use thread groups or something like that to accomplish a nice report like this
    * Lint and descriptions
    * Create a temporary randomized directory to store the frames while processing so that we can safely cleanup afterwards
        * Add an option to keep the frames if the user wants to. In which case they would have to specify a directory in which to keep the frames
    * Check and report if ffmpeg is installed on the system before starting
    * Arguments to control the settings used by ffmpeg and waifu (especially waifu)

Scripsit Autem Mortale, Leviter Calcare
"""

### IMPORTS ###

import argparse
import os
import subprocess
import threading
import time



### PARAMETERS & CONSTANTS ###

FFMPEG_COMMAND = "ffmpeg"
WAIFU_PATH = "waifu2x-mac-master/build/waifu2x-mac-app.app/Contents/MacOs/waifu2x"
FRAME_STORE = "tempFrames" # Directory where the frames should be stored by FFMPEG and waifu2x
DEFAULT_OUTPUT = "output.mkv"
DEFAULT_THREADS = int(max(round(os.cpu_count() / 2), 1))



### CLASSES ###

class frame_counter():
    """"""

    def __init__(self):
        """"""
        self.counter_semaphore = threading.Semaphore()
        self.current_count = 1

    def get_next_frame(self):
        """"""
        self.counter_semaphore.acquire()
        result = self.current_count
        self.current_count += 1
        self.counter_semaphore.release()
        return result

    def get_current_frame(self):
        """"""
        self.counter_semaphore.acquire()
        result = self.current_count
        self.counter_semaphore.release()
        return result



### FUNCTIONS ###

def waifu2x_worker(number, waifu_path, counter):
    """"""

    while True:
        frame_number = counter.get_next_frame()
        frame_path = os.path.join(FRAME_STORE, "frame-{0}.png".format(frame_number))
        pframe_path = os.path.join(FRAME_STORE, "processedFrame-{0}.png".format(frame_number))

        # Check if the frame exists so we know when to stop and so we don't upset waifu
        if os.path.exists(frame_path):
            print("Worker #{0} will process frame {1}".format(number, frame_number))
            subprocess.run([waifu_path, "--scale", "2", "--noise", "4", "--input", frame_path, "--output", pframe_path],
                stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        else:
            break # This will stop the thread



### SCRIPT ###

def cli():
    """Command Line Interface"""

    parser = argparse.ArgumentParser(description="Upscale your videos with the power of waifus!",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-w', '--waifu-path', type=str,
                        default="waifu2x-mac-master/build/waifu2x-mac-app.app/Contents/MacOs/waifu2x",
                        help="Path to the waifu2x CLI executable")
    parser.add_argument('-t', '--threads', type=int, default=DEFAULT_THREADS,
                        help="Amount of threads to spawn off for waifu2x, excluding the main thread (FFMPEG will use all threads)")
    parser.add_argument('-i', '--input', type=str, required=True,
                        help="Input video file")
    parser.add_argument('-o', '--output', type=str, default=DEFAULT_OUTPUT,
                        help="Output video file".format(DEFAULT_OUTPUT))

    return parser.parse_args()

def main(args):
    """"""

    # Create a directory to hold the frames
    if not os.path.exists(FRAME_STORE):
        os.mkdir(FRAME_STORE)

    # Separate the frames using ffmpeg
    print("Separating the frames...\n")
    subprocess.run([FFMPEG_COMMAND, "-i", args.input, os.path.join(FRAME_STORE, "frame-%d.png")])

    # Multi-threaded waifu upscaling
    start_time = time.perf_counter()
    counter = frame_counter()
    thread_list = []
    print("\nCalling your friendly neighborhood waifus...")
    for i in range(args.threads):
        new_thread = threading.Thread(target=waifu2x_worker, args=(i, args.waifu_path, counter))
        thread_list.append(new_thread)
        new_thread.start()

    # Wait for all the threads to finish
    for a_thread in thread_list:
        a_thread.join()

    stop_time = time.perf_counter()
    print("All waifus are done!")

    # Print some fun information about the performance
    perf_time = int((stop_time - start_time))
    print("Processed about {0} frames in {1} seconds (avg. {2:.2f} seconds per frame)".format(
        counter.get_current_frame(), perf_time, perf_time/counter.get_current_frame()))

    print("Processing final video...")
    subprocess.run([FFMPEG_COMMAND, "-i", os.path.join(FRAME_STORE, "processedFrame-%d.png"), args.output])

    print("ALL DONE! ENJOY!")


if __name__ == "__main__":
    main(cli())
